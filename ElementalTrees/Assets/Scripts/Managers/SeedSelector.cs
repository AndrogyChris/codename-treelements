using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SeedSelector : MonoBehaviour
{
    [SerializeField] GameObject neutral, fire, air, water, earth;

    public static UnityEvent<GameObject> OnSeedSelect = new UnityEvent<GameObject>();

    public void ChooseNeutral()
    {
        OnSeedSelect.Invoke(neutral);
    }
    public void ChooseFire()
    {
        OnSeedSelect.Invoke(fire);
    }
    public void ChooseAir()
    {
        OnSeedSelect.Invoke(air);
    }
    public void ChooseWater()
    {
        OnSeedSelect.Invoke(water);
    }
    public void ChooseEarth()
    {
        OnSeedSelect.Invoke(earth);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


public class InputManager : MonoBehaviour
{
    public static UnityEvent<Vector2> OnMoveInput = new UnityEvent<Vector2>();
    public static UnityEvent<bool> OnToolScrollInput = new UnityEvent<bool>();
    public static UnityEvent OnInteractInput = new UnityEvent();


    PlayerInputs inputs;

    private void Awake()
    {
        inputs = new PlayerInputs();

        inputs.player.movement.performed += MoveInput;
        inputs.player.movement.canceled += MoveInput;
        inputs.player.scroll.performed += ToolScrollInput;

        inputs.player.interact.performed += x => OnInteractInput.Invoke();
    }

    private void MoveInput(InputAction.CallbackContext ctx)
    {
        Vector2 moveInput = ctx.ReadValue<Vector2>();
        OnMoveInput.Invoke(moveInput);
    }

    private void ToolScrollInput(InputAction.CallbackContext ctx)
    {
        float value = ctx.ReadValue<float>();

        if (value != 0)
        {
            if (value > 0)
            {
                OnToolScrollInput.Invoke(false);
            }
            else
            {
                OnToolScrollInput.Invoke(true);
            }
        }
    }

    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }
}

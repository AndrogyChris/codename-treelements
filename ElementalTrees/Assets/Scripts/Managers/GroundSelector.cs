using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GroundSelector : MonoBehaviour
{
    [SerializeField] GroundObject neutral, fire, air, water, earth;

    public static UnityEvent<GroundObject> OnGroundSelect = new UnityEvent<GroundObject>();

    public void ChooseNeutral()
    {
        OnGroundSelect.Invoke(neutral);
    }
    public void ChooseFire()
    {
        OnGroundSelect.Invoke(fire);
    }
    public void ChooseAir()
    {
        OnGroundSelect.Invoke(air);
    }
    public void ChooseWater()
    {
        OnGroundSelect.Invoke(water);
    }
    public void ChooseEarth()
    {
        OnGroundSelect.Invoke(earth);
    }
}

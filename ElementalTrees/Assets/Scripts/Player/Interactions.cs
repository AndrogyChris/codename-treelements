using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactions : MonoBehaviour
{
    public bool canPlant;
    public bool canPrep;

    public bool canChop;
    public bool canHarvest;

    public Transform plantLocation;


    public Ground ground;

    private void OnTriggerStay2D(Collider2D collision)
    {
        ground = collision.GetComponent<Ground>();
        if (collision.CompareTag("Interactable") && !ground.hasTree)
        {
            canPlant = true;
            canPrep = true;
            plantLocation = collision.transform;
        }
        else if (collision.CompareTag("Interactable") && ground.hasTree)
        {
            if (!ground.growing)
            {
                canChop = true;
                canHarvest = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Interactable"))
        {
            canPrep = false;
            canPlant = false;
            canChop = false;
            canHarvest = false;
        }
    }
}

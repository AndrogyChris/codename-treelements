using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [Header("Sprites")]
    [SerializeField] GameObject frontSprite;
    [SerializeField] GameObject backSprite;
    [SerializeField] GameObject sideSprite;

    [Header("Movement")]
    [SerializeField] float moveSpeed;
    //Vector2 movementInput;

    bool isFacingRight = true;
    Rigidbody2D rb2D;

    private void Awake()
    {
        //Componenet References
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        InputManager.OnMoveInput.AddListener(Move);
    }

    private void FixedUpdate()
    {
        DetermineFacingDirection();
    }

    private void Move(Vector2 moveInput)
    {
        rb2D.velocity = moveInput * moveSpeed * Time.fixedDeltaTime;
    }

    private void DetermineFacingDirection()
    {
        if (rb2D.velocity.x != 0)
        {
            frontSprite.SetActive(false);
            backSprite.SetActive(false);
            sideSprite.SetActive(true);
        }

        if (rb2D.velocity.y > 0)
        {
            frontSprite.SetActive(false);
            backSprite.SetActive(true);
            sideSprite.SetActive(false);
        }
        else if (rb2D.velocity.y < 0)
        {
            frontSprite.SetActive(true);
            backSprite.SetActive(false);
            sideSprite.SetActive(false);
        }

        if (rb2D.velocity.x < 0 && isFacingRight)
        {
            FlipSprite();
        }
        if (rb2D.velocity.x > 0 && !isFacingRight)
        {
            FlipSprite();
        }
    }

    private void FlipSprite()
    {
        isFacingRight = !isFacingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}

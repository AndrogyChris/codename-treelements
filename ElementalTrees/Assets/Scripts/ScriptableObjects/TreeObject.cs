
using UnityEngine;

public enum GroundElement
{
    Neutral,
    Fire,
    Air,
    Water,
    Earth
}

[CreateAssetMenu(fileName = "Seed Object", menuName = "Seed Data", order = 0)]
public class TreeObject : ScriptableObject
{
    public new string name;
    public GroundElement element;

    public GameObject sapplingImage;
    public GameObject grownImage;
    public GameObject bareImage;

    public float growTimeInfluence;
}

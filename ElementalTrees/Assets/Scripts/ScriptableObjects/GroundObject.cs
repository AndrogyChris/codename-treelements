using UnityEngine;

public enum Element
{
    Neutral,
    Fire,
    Air,
    Water,
    Earth
}

[CreateAssetMenu(fileName = "Ground Object", menuName = "Ground Data", order = 0)]
public class GroundObject : ScriptableObject
{
    public new string name;
    public Element element;

    public GameObject groundSprite;
    public float growTimeInfluence;
}

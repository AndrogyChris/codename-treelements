using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public bool hasTree;

    public bool growing; 

    public GameObject sprite;
    public GroundObject groundObject;

    private void Start()
    {
        ChangeGround();
    }

    public void ChangeGround()
    {
        if (sprite.transform.childCount != 0)
        {
            Destroy(sprite.transform.GetChild(0).gameObject);
        }
        Instantiate(groundObject.groundSprite, transform.position, Quaternion.identity, sprite.transform);
    }

    public void RemoveTree()
    {
        if (transform.childCount == 2)
        {
            Destroy(transform.GetChild(1).gameObject);
        }
    }

}

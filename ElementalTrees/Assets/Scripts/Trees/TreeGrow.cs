using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGrow : MonoBehaviour
{
    [SerializeField] TreeObject tree;
    [SerializeField] float growTime;

    GameObject sapplingSprite;
    GameObject grownSprite;
    GameObject bareSprite;

    Ground ground;
    GrowTimerUI growTimer;

    private void Awake()
    {
        growTimer = FindObjectOfType<GrowTimerUI>();
    }

    private void Start()
    {
        sapplingSprite = Instantiate(tree.sapplingImage, transform.position, Quaternion.identity, transform);
        grownSprite = Instantiate(tree.grownImage, transform.position, Quaternion.identity, transform);
        bareSprite = Instantiate(tree.bareImage, transform.position, Quaternion.identity, transform);
        bareSprite.SetActive(false);
        grownSprite.SetActive(false);

        ground = GetComponentInParent<Ground>();
        growTime = growTime + tree.growTimeInfluence + ground.groundObject.growTimeInfluence;

        StartCoroutine(GrowCoroutine());
    }

    public void GetHarvested()
    {
        StartCoroutine(RegrowCoroutine());
    }

    IEnumerator GrowCoroutine()
    {
        //growTimer.ShowGrowTimer(transform);
        ground.growing = true;

        yield return new WaitForSeconds(growTime);

        ground.growing = false;
        sapplingSprite.SetActive(false);
        grownSprite.SetActive(true);
    }

    IEnumerator RegrowCoroutine()
    {
        grownSprite.SetActive(false);
        bareSprite.SetActive(true);
        ground.growing = true;

        yield return new WaitForSeconds(growTime);

        ground.growing = false;
        bareSprite.SetActive(false);
        grownSprite.SetActive(true);
    }
}

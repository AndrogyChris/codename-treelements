using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InteractUI : MonoBehaviour
{
    ToolPlant toolPlant;
    ToolChop toolChop;
    ToolPrepare toolPrep;
    ToolGather toolGather;

    [SerializeField] GameObject plantImage;
    [SerializeField] GameObject prepImage;
    [SerializeField] GameObject chopImage;
    [SerializeField] GameObject gatherImage;
    
    TextMeshProUGUI text;

    Transform target;
    Camera mainCamera;
    Interactions interactions;

    private void Awake()
    {
        mainCamera = Camera.main;
        interactions = FindObjectOfType<Interactions>();

        toolPlant = FindObjectOfType<ToolPlant>();
        toolChop = FindObjectOfType<ToolChop>();
        toolGather = FindObjectOfType<ToolGather>();
        toolPrep = FindObjectOfType<ToolPrepare>();

        text = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void LateUpdate()
    {
        if (interactions.canPlant)
        {
            target = interactions.plantLocation;
            transform.position = mainCamera.WorldToScreenPoint(target.position);

            if (toolPlant.equipped)
            {
                plantImage.SetActive(true);
                prepImage.SetActive(false);
                gatherImage.SetActive(false);
                chopImage.SetActive(false);
            }
            if (toolPrep.equipped)
            {
                prepImage.SetActive(true);
                plantImage.SetActive(false);
                gatherImage.SetActive(false);
                chopImage.SetActive(false);
            }
        }

        if (interactions.canChop)
        {
            target = interactions.plantLocation;
            transform.position = mainCamera.WorldToScreenPoint(target.position);

            if (toolGather.equipped)
            {
                gatherImage.SetActive(true);
                plantImage.SetActive(false);
                prepImage.SetActive(false);
                chopImage.SetActive(false);
            }
            if (toolChop.equipped)
            {
                chopImage.SetActive(true);
                plantImage.SetActive(false);
                prepImage.SetActive(false);
                gatherImage.SetActive(false);
            }
        }

        if (!interactions.canChop && !interactions.canPlant)
        {
            plantImage.SetActive(false);
            prepImage.SetActive(false);
            gatherImage.SetActive(false);
            chopImage.SetActive(false);
        }
    }
}

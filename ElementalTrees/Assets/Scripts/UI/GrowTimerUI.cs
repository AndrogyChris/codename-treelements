using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GrowTimerUI : MonoBehaviour
{
    [SerializeField] GameObject growDisplayImage;
    GameObject display;

    Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    public void ShowGrowTimer(Transform target)
    {
        Instantiate(growDisplayImage, mainCamera.WorldToScreenPoint(target.position), Quaternion.identity, transform);
    }

}

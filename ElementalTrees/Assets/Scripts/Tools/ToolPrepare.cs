using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolPrepare : ToolBase
{
    Interactions interactions;

    [SerializeField] GroundObject currentGroundObject;

    private void Awake()
    {
        interactions = GetComponentInParent<Interactions>();
    }

    private void OnEnable()
    {
        GroundSelector.OnGroundSelect.AddListener(SelectGround);
    }

    public override void EquipTool()
    {
        base.EquipTool();
    }

    public override void UseTool()
    {
        if (interactions.canPrep)
        {
            Debug.Log($"Using {this.name}");
            interactions.ground.groundObject = currentGroundObject;
            interactions.ground.ChangeGround();
        }
    }

    private void SelectGround(GroundObject element)
    {
        currentGroundObject = element;
    }
}

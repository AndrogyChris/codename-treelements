using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolGather : ToolBase
{
    Interactions interactions;


    private void Awake()
    {
        interactions = GetComponentInParent<Interactions>();
    }

    public override void EquipTool()
    {
        base.EquipTool();
    }

    public override void UseTool()
    {
        interactions.ground.GetComponentInChildren<TreeGrow>().GetHarvested();
        Debug.Log($"Using {this.name}");
    }
}

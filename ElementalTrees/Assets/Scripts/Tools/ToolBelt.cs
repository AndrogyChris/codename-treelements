using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolBelt : MonoBehaviour
{
    [SerializeField] public List<ToolBase> tools = new List<ToolBase>();

    [SerializeField] public ToolBase currentTool;

    [SerializeField] bool usingTool = false;

    public int currentIndex;

    private void OnEnable()
    {
        InputManager.OnInteractInput.AddListener(UseToolStarted);
        InputManager.OnToolScrollInput.AddListener(CycleTool);
    }

    private void Start()
    {
        if (tools.Count > 0)
        {
            currentTool = tools[0];
            currentTool.EquipTool();
        }
    }

    private void CycleTool(bool next)
    {
        currentIndex = tools.IndexOf(currentTool);

        if (next) currentIndex++;
        else currentIndex--;

        if (currentIndex > tools.Count - 1) currentIndex = 0;
        else if (currentIndex < 0) currentIndex = tools.Count - 1;

        currentTool.UnequipTool();
        currentTool = tools[currentIndex];
        currentTool.EquipTool();
    }

    private void UseToolStarted()
    {
        //usingTool = true;
        //StartCoroutine(UsingTool());
        currentTool.UseTool();
    }

    private void UseToolCancelled()
    {
        usingTool = false;
    }

    IEnumerator UsingTool()
    {
        float waitTime = currentTool.useTime;

        while (usingTool)
        {
            if (waitTime > 0)
            {
                waitTime -= Time.deltaTime;
            }
            else
            {
                waitTime = currentTool.useTime;
                currentTool.UseTool();
            }
            yield return null;
        }
    }
}

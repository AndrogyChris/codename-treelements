using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ToolBase : MonoBehaviour
{
    public bool equipped = false;
    public float useTime = 1;

    public GameObject highlighUI;
    public virtual void EquipTool()
    {
        equipped = true;
        highlighUI.SetActive(true);
        Debug.Log($"Equipping {this.name}");
    }

    public virtual void UnequipTool()
    {
        equipped = false;
        highlighUI.SetActive(false);
        Debug.Log($"Unequipping {this.name}");
    }

    public abstract void UseTool();

}

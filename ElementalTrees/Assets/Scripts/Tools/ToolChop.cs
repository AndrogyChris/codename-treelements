using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolChop : ToolBase
{
    Interactions interactions;


    private void Awake()
    {
        interactions = GetComponentInParent<Interactions>();
    }

    public override void EquipTool()
    {
        base.EquipTool();
    }

    public override void UseTool()
    {
        if (interactions.canChop)
        {
            interactions.ground.RemoveTree();
            interactions.ground.hasTree = false;
            Debug.Log($"Using {this.name}");
        }
        
    }
}

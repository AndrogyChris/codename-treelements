using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolPlant : ToolBase
{
    Interactions interactions;

    [SerializeField] GameObject currentSeed;


    private void Awake()
    {
        interactions = GetComponentInParent<Interactions>();
    }

    private void OnEnable()
    {
        SeedSelector.OnSeedSelect.AddListener(SwitchElement);
    }

    public override void EquipTool()
    {
        base.EquipTool();
    }

    public override void UseTool()
    {
        if (interactions.canPlant)
        {
            Debug.Log($"using {this.name}");
            Instantiate(currentSeed, interactions.plantLocation.position, Quaternion.identity, interactions.plantLocation);
            interactions.canPlant = false;
            interactions.canPrep = false;
            interactions.ground.hasTree = true;
        }
    }

    private void SwitchElement(GameObject element)
    {
        currentSeed = element;
    }
}
